import React from "react"
import {IntlProvider} from 'react-intl'
import French from '../../lang/fr.json'
import English from '../../lang/en.json'


const locale = navigator.language;

let lang;
if (locale==="en") {
   lang = English;
} else if (locale === "fr") {
       lang = French;
   } 


const Locales = ({children}) => {
  return (
	<IntlProvider messages={French} locale={locale} defaultLocale="en">
        {children}
	</IntlProvider>

  );
}

export default Locales;
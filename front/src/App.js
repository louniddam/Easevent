import './styles/App.css';
import Locales from "../src/ui-components/Locales/"
import React from "react"
import { FormattedMessage }from "react-intl"
import Routes from "../src/routes/"

function App() {
  return (
	<React.Fragment>
		<Locales>
			<div className="App">
				<p>Easevent</p>
				<FormattedMessage id="tr.I try" />
			</div>
		</Locales>
	</React.Fragment>
  );
}

export default App;

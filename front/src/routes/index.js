import {useRoutes} from "react-router-dom"

const ThemeRoutes = () => {
    return useRoutes([])
}

export default ThemeRoutes